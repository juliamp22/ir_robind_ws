#include <ros/ros.h>
#include <moveit/move_group_interface/move_group_interface.h>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "exercise1");
  // start a background "spinner", so our node can process ROS messages
  // - this lets us know when the move is completed
  ros::AsyncSpinner spinner(1);
  spinner.start();
  // Your code goes here
  moveit::planning_interface::MoveGroupInterface move_group("manipulator");
  move_group.setRandomTarget();
  move_group.move();
}
