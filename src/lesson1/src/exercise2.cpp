#include <ros/ros.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/PoseArray.h>
#include <vector>
#include <unistd.h>

// Your code goes here

int main(int argc, char **argv)
{
  ros::init(argc, argv, "exercise1");

  // start a background "spinner", so our node can process ROS messages

  // -this lets us know when the move is completed
  ros::AsyncSpinner spinner(1);
  spinner.start();

  // Your code goes here
  // creem variable move_group del tipus MoveGroupInterface
  moveit::planning_interface::MoveGroupInterface move_group("manipulator");
  std::cout << "Menu: " << "\n";
  std::cout << "1: get robot positions. " << "\n";
  std::cout << "2: execute Julia's first letter. " << "\n";
  // draw the first letter of your name
  geometry_msgs::Pose pose;

  //getting number:
  int number;
  std::map<std::string, double> joints;
  geometry_msgs::PoseArray vector_pose;
  vector_pose.header.stamp = ros::Time::now();
  int i = 0;
  geometry_msgs::PoseStamped get_pose;
  std::vector<double> get_joints;
  while (number!=-1){
    std::cout << "Introduce number: ";
    std::cin >> number;
  //std::cout << "Introduce number: " << number;
    switch (number) {
      case 1:
          std::cout << "1";   //execution starts at this case label
          get_pose =  move_group.getCurrentPose();
          get_joints = move_group.getCurrentJointValues();
        //  vector_pose.poses.push_back(get_pose.pose);
          for (int i = 0; i< get_joints.size(); ++i) std::cout << "joint " << i << ": " << get_joints[i] << std::endl;
        break;
      case 2:
        std::vector<std::string> names;
        names = {"Julia-1","Julia-2","Julia-3","Julia-4","Julia-5","Julia-6"};
        for (int j = 0; j < names.size(); ++j){
          move_group.setNamedTarget(names[j]);
          move_group.move();
          usleep(1000000);
          //ros::Duration(1).sleep();
        }

        std::cout << "Finished" << "\n";
      break;

      }

  }

}
