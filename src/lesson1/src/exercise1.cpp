#include <ros/ros.h>
#include <moveit/move_group_interface/move_group_interface.h>
#include <geometry_msgs/Pose.h>

#include <vector>
// Your code goes here

int main(int argc, char **argv)
{
  ros::init(argc, argv, "exercise1");

  // start a background "spinner", so our node can process ROS messages

  // -this lets us know when the move is completed
  ros::AsyncSpinner spinner(1);
  spinner.start();

// creem variable move_group del tipus MoveGroupInterface
moveit::planning_interface::MoveGroupInterface move_group("manipulator");


//A menu is shown in a loop execution. If the user enters the number 1, the robot will
//move to a defined pose (1).
//If the user enters the number 2, the robot will move to a positions defined by its
//joints (2).
//If the user enters the number 3, the robot will move to a specified Cartesian
//position (3).
//If the user enters the number 4, the robot will move to a random target.
//If the user enters the number 5, the current position of the robot will be printed in
//the terminal. The position must be printed in Cartesian and in joint values (if you
//program that the joint values are printed in degrees instead of radians you will do
//future work).
//Number -1 will end the program.

std::cout << "Menu: " << "\n";
std::cout << "1: robot will move to defined pose. " << "\n";
std::cout << "2: robot will move to a positions defined by its joints. " << "\n";
std::cout << "3: robot will move to specified Cartesian position. " << "\n";
std::cout << "4: robot will move to a random target. " << "\n";
std::cout << "5: the current position of the robot will be printed in the terminal. " << "\n";
std::cout << "-1: Number -1 will end the program. " << "\n";

int number;
while (number!=-1){
std::cout << "Introduce number: ";
std::cin >> number;

//std::cout << "Introduce number: " << number;

geometry_msgs::Pose pose;
std::map<std::string, double> joints;
switch (number) {
        case 1: //Especifiquem valor de les joints directament
          pose.position.x = 0.94; // say desired x-coord is 1
          pose.position.y = 0.5;
          pose.position.z = 1.455; // let's hope so!
          pose.orientation.x = 0.0; //always, for motion in horizontal plane
          pose.orientation.y = 0.0; // ditto
          pose.orientation.z = 0.0; // implies oriented at yaw=0, i.e. along x axis
          pose.orientation.w = 1.0; //sum of squares of all components of unit quaternion i
          move_group.setPoseTarget(pose);
          move_group.move();
          std::cout << "Case1: Setting pose Target" << "\n";
          break;
        case 2: std::cout << "2";   //execution starts at this case label
          joints["joint_1"] = 0.00;
          joints["joint_2"] = -0.48;
          joints["joint_3"] = 0.00;
          joints["joint_4"] = -0.90;
          joints["joint_5"] = 0.00;
          joints["joint_6"] = -0.50;
          move_group.setJointValueTarget(joints);
          move_group.move();
          std::cout << "Case2: Setting joints Target"<< "\n";
          break;
        case 3:
          //Anem al punt HOME
          move_group.setNamedTarget("State_1");
          move_group.move();
          std::cout << "Case3: Setting all-zeros Target"<< "\n";
          break;
        case 4:
          move_group.setRandomTarget();
          move_group.move();//Es mou al punt random creat
          std::cout << "Case4: Setting random Target"<< "\n";
          break;
        case 5:
          std::cout << "Pose: "<< move_group.getCurrentPose() << "\n"; //cartesiana
          //std::cout << "GetJoints" << move_group.getCurrentJointValues() << "\n"; //rad
          std::vector<double> joints = move_group.getCurrentJointValues();
          for (int i = 0; i< joints.size(); ++i) std::cout << "joint " << i << " degrees:" <<" " << joints[i]*180/M_PI << std::endl;
          break;
    }

//especifiquem un punt random

}

}
